function [ out ] = computeclassvariance(dataSet)

    nClasses = size(dataSet, 3);
    for i = 1:nClasses;
        out(i, :) = var(dataSet(:, :, i));
    end

end

