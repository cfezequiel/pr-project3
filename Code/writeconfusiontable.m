function [ output_args ] = writeconfusiontable(fn, title, classes, errors)
%WRITECONFUSIONTABLE Write confusion table to CSV file

    % Parse input
    errorTypeI = errors.errorTypeI;
    errorTypeII = errors.errorTypeII;
    tally = errors.classTally;
    
    % Open file
    fid = fopen(fn, 'w');
    
    % Write column headers
    fprintf(fid, 'Confusion Table\n');
    fprintf(fid, '"%s"\n', title);
    fprintf(fid, 'Classified as:,');
    nClasses = size(classes, 2);
    for i = 1:nClasses
        fprintf(fid, '%s,', classes(i));
    end
    fprintf(fid, 'Error\n');
    fprintf(fid, 'True class,');
    for i = 1:nClasses
        fprintf(fid, ',');
    end
    fprintf(fid, 'Type I\n');
    
    % Write row headers and contents
    [m n] = size(tally);
    for i = 1:m
        fprintf(fid, '%s,', classes(i));
        for j = 1:n
            fprintf(fid, '%d,', tally(i,j));
        end
        fprintf(fid, '%d\n', errorTypeI(i));
    end
    
    % Write last row
    fprintf(fid, 'Error Type II,');
    for i = 1:m
        fprintf(fid, '%d,', errorTypeII(i));
    end
    fprintf(fid, '%d\n', sum(errorTypeII));
            
    % Close file
    fclose(fid);
end

