function [ out ] = computetrainingsetncm(ncmFeatures, classes)
%COMPUTETRAININGSET Extract training set data from input dataset file.

    % Partition the normalized central moments into a set of matrices
    ncmSlices = splitrows(ncmFeatures, size(classes, 2));

    % Compute the class means
    classMeanValues = computeclassmean(ncmSlices);

    % Compute the class variances
    classVarianceValues = computeclassvariance(ncmSlices);

    % Compute the covariance matrix for each class
    covMatrixSlices = computecovariance(ncmSlices);

    % Compute the inverse of the covariance matrix for each class
    invCovMatrixSlices = invdataset(covMatrixSlices);

    % Compute the average covariance matrix across all classes
    aveCovMatrix = computeaveragecovariance(covMatrixSlices);

    % Compute the inverse average covariance matrix
    invAveCovMatrix = inv(aveCovMatrix);
    
    % Set output
    out.means = classMeanValues;
    out.variances = classVarianceValues;
    out.cov = covMatrixSlices;
    out.aveCov = aveCovMatrix;
    
end

