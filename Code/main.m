% 
% CAP 6638 Pattern Recognition - Project 3
%
% Date: 2012-04-20
% 
% Authors:
%   Sinan Onal
%   Carlos Ezequiel
%

clear all;
clc;

% Parameters
nDatasets = 4;
nClassifiers = 4;

% Extract features from the sample datasets (A, B, C, D) respectively
pqIndices = [0 0; 0 2; 0 3; 1 1; 1 2; 2 0; 2 1; 3 0];
datasets = cell(nDatasets);
datasets{1} = extractfeatures('data_setA.dat', pqIndices, []);
overallRms = datasets{1}.overallRms;
disp(overallRms);
datasets{2} = extractfeatures('data_setB.dat', pqIndices, overallRms);
datasets{3} = extractfeatures('data_setC.dat', pqIndices, overallRms);
datasets{4} = extractfeatures('data_setD.dat', pqIndices, overallRms);

% Get training set class vector
classes = 'acemnorsxz';
nClasses = size(classes, 2);
nSamples = size(datasets{1}.bin, 1);
nSamplesPerClass = nSamples/nClasses;
classIndices = 1:nClasses;
group = ones(nSamples, 1);
for i = 1:nClasses;
    min = (i - 1) * nSamplesPerClass + 1;
    max = i * nSamplesPerClass; 
    group(min:max, 1) = group(min:max, 1) * i; 
end;

% Run the classifiers
classifierData = cell(nClassifiers, nDatasets);
for j = 1:nDatasets
    classifierData{1, j} = knnclassifier(datasets{j}.bin, ...
                                         datasets{1}.bin, ...
                                         group, 1);
    classifierData{2, j} = knnclassifier(datasets{j}.bin, ...
                                         datasets{1}.bin, ...
                                         group, 5);  
    classifierData{3, j} = knnclassifier(datasets{j}.ncm, ...
                                         datasets{1}.ncm, ...
                                         group, 1);
    classifierData{4, j} = knnclassifier(datasets{j}.ncm, ...
                                         datasets{1}.ncm, ...
                                         group, 5);                                      
end


% Verify classifications and collect error data
errorData = cell(nClassifiers, nDatasets);
for i = 1:nClassifiers
    for j = 1:nDatasets
        errorData{i, j} = verifyclassifications(classifierData{i, j}, ...
                                                classes);
    end
end

% Write the summary table
outputDir = '../Output';
datasetNames = 'ABCD';
filename = sprintf('%s/summarytable.csv', outputDir);
writesummarytable(filename, errorData, datasetNames);
   
% Write the confusion tables (classifier 1 only)
classifierNames = ...
   {'Method 1 - NN on the bitmaps of the samples, with Euclidean metric', ...
    'Method 2 - 5-NN on the bitmaps of the samples, with Euclidean metric', ...
    'Method 3 - NN on the eight moment features, with Euclidean metric' ...
    'Method 4 - 5-NN on the eight moment features, with Euclidean metric'};
    
for i = 2:nDatasets
    filename = sprintf('%s/conftable_%s1.csv', outputDir, datasetNames(i));
    title = sprintf('%s (trained on A, tested on %s)', ...
        classifierNames{1}, datasetNames(i));
    writeconfusiontable(filename, title, classes, errorData{1, i});
end
for i = 2:nDatasets
    filename = sprintf('%s/conftable_%s2.csv', outputDir, datasetNames(i));
    title = sprintf('%s (trained on A, tested on %s)', ...
        classifierNames{2}, datasetNames(i));
    writeconfusiontable(filename, title, classes, errorData{2, i});
end

disp('Done.');
