function [ out ] = computecovariance(dataSet)

    samplesPerClass = size(dataSet, 1);
    nClasses = size(dataSet, 3);
    I = ones(samplesPerClass, 1);
    for i = 1:nClasses
        partial = dataSet(:, :, i) - ...
                  ((I * I' * dataSet(:, :, i) * 1 / samplesPerClass));
        out(:, :, i) = 1 / samplesPerClass * partial' * partial;
    end

end



  



