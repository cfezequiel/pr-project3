% This function is adapted from the 'cvKnn' function of 
% Naotoshi Seo <sonots(at)sonots.com>
% http://note.sonots.com/SciSoftware/knn.html

% Synopsis
%   [Class] = cvKnn(X, Proto, ProtoClass, [K], [distFunc])
%
% Description
%   K-Nearest Neighbor classification
%
% Inputs ([]s are optional)
%   (matrix) Sample   D x N matrix representing column classifiee vectors
%                     where D is the number of dimensions and N is the
%                     number of vectors.
%   (matrix) Training D x P matrix representing column prototype vectors
%                     where D is the number of dimensions and P is the
%                     number of vectors.
%   (vector) Group
%                     1 x P vector containing class lables for prototype
%                     vectors. 
%   (scalar) [k = 1]  K-NN's K. Search K nearest neighbors.
%   (func)   [distFunc = @pdist2]
%                     A function handle for distance measure. The function
%                     must have two arguments for matrix X and Y. See
%                     cvEucdist.m (Euclidean distance) as a reference.
%
% Outputs ([]s are optional)
%   (vector) out    structure containing:
%                   .classifications
%                       1 x N vector containing classified class labels 
%                       for X. Class(n) is the class id for X(:,n). 
%
function [out] = knnclassifier(Sample, Training, Group, k, distance)
if ~exist('k', 'var') || isempty(k)
    k = 1;
end
if ~exist('distance', 'var') || isempty(distance)
    distance = @pdist2;
end
if size(Sample, 1) ~= size(Training, 1)
    error('Dimensions of classifier vectors and prototype vectors do not match.');
end

% Calculate euclidean distances between samples and training data
d = distance(Sample, Training);

if k == 1
    % Get minimum across columns
    [minVal, I] = min(d, [], 2);
    classifications = Group(I);
else
    % Sort across columns
    [sorted, I] = sort(d, 2);
    % Get the k-closest neighbors
    closestNeighbors = I(:, 1:k);
    knnClasses = Group(closestNeighbors);
    classifications = mode(knnClasses, 2);    
end

out.classifications = classifications;
